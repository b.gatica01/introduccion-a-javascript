var btn = document.getElementById("btn");
var resultado = document.getElementById("resultado")
var inputUno = document.getElementById("input-uno");
var inputDos = document.getElementById("input-dos");
btn.addEventListener("click", function () {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    if (parseInt(n1) % 1 == 0 && parseInt(n2) % 1 == 0 && parseInt(n1) <= parseInt(n2)) {
        
        resultado.innerHTML = "Numero random en el rango:"+random(n1, n2);
    } else {
        alert("Ingrese 2 numeros enteros, el primero debe ser mayor o igual que el segundo")
    }
});
/* Función que retorna el numero random */

function random(n1, n2) {
    return Math.floor(Math.random() * (parseInt(n2) - parseInt(n1)) ) + parseInt(n1);
  }