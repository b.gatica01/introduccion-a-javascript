// Obtenemos el button y lo almacenamos en una variable llamada "btn"
var btnSuma = document.getElementById("btnSuma");
var btnResta = document.getElementById("btnResta");
var btnMultiplicacion = document.getElementById("btnMultiplicacion");
var btnDivision = document.getElementById("btnDivision");
/* Obtenemos el div que muestra el resultado y lo
almacenamos en una variable llamada "resultado" */
var resultado = document.getElementById("resultado")
/* Obtenemos los dos input y los almacenamos en
variables "inputUno" y "inputDos" */
var inputUno = document.getElementById("input-uno");
var inputDos = document.getElementById("input-dos");
// Añadimos el evento click a la variable "btn"
btnSuma.addEventListener("click", function () {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    if (parseInt(n1) % 1 == 0 && parseInt(n2) % 1 == 0 ) {
        
        resultado.innerHTML = suma(n1, n2);
    } else {
        alert("Ingrese 2 numeros enteros")
    }
});

btnResta.addEventListener("click", function () {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    if (parseInt(n1) % 1 == 0 && parseInt(n2) % 1 == 0 ) {
        
        resultado.innerHTML = resta(n1, n2);
    } else {
        alert("Ingrese 2 numeros enteros")
    }
});

btnMultiplicacion.addEventListener("click", function () {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    if (parseInt(n1) % 1 == 0 && parseInt(n2) % 1 == 0 ) {
        
        resultado.innerHTML = multiplicacion(n1, n2);
    } else {
        alert("Ingrese 2 numeros enteros")
    }
});

btnDivision.addEventListener("click", function () {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    if (parseInt(n1) % 1 == 0 && parseInt(n2) % 1 == 0 && parseInt(n2) != 0) {
        
        resultado.innerHTML = division(n1, n2);
    } else {
        alert("Ingrese 2 numeros enteros distintos de 0")
    }
});

/* Función que retorna la suma de dos números */
function suma(n1, n2) {
    // Es necesario aplicar parseInt a cada número
    return parseInt(n1) + parseInt(n2);
}

function resta(n1, n2) {
    return parseInt(n1) - parseInt(n2);
}

function multiplicacion(n1, n2) {
    return parseInt(n1) * parseInt(n2);
}

function division(n1, n2) {
    return parseInt(n1) / parseInt(n2);
}